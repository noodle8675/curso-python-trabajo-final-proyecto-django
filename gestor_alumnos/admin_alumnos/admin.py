from django.contrib import admin
from .models import Alumno, Curso, BandaHoraria

# Register your models here.
class AlumnoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'apellido', 'dni', 'telefono', 'correo_electronico', 'curso',]
class CursoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'descripcion', 'banda_horaria', 'nota',]
class BandaHorariaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'horario_inicio', 'horario_fin',]

admin.site.register(Alumno, AlumnoAdmin)
admin.site.register(Curso, CursoAdmin)
admin.site.register(BandaHoraria, BandaHorariaAdmin)
